package com.williamhill.gamingusers.users.presentation.viewmodel

import app.cash.turbine.test
import com.williamhill.gamingusers.core.data.UsersRepository
import com.williamhill.gamingusers.users.presentation.contract.UsersIntent
import com.williamhill.gamingusers.users.presentation.contract.UsersViewEvent
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestDispatcher
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test
import kotlin.test.assertEquals

@OptIn(ExperimentalCoroutinesApi::class)
class UsersViewModelTest {

    private val testDispatcher: TestDispatcher = UnconfinedTestDispatcher()
    private val usersRepositoryMock = mockk<UsersRepository>(relaxed = true)
    private val tested by lazy { UsersViewModel(usersRepositoryMock) }

    @Before
    fun setUp() {
        Dispatchers.setMain(testDispatcher)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun `emits NavigateToUser on the NavigateToUser intent`() = runTest(testDispatcher) {
        tested.uiEvent.test {
            val expectedResult = UsersViewEvent.NavigateToUser(A_USER_ID)

            tested.handleIntent(UsersIntent.NavigateToUser(A_USER_ID))

            assertEquals(expectedResult, awaitItem())
        }
    }

    private companion object {

        const val A_USER_ID = "123"
    }
}
