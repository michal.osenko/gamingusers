package com.williamhill.gamingusers

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class GamingUsersApp : Application()
