package com.williamhill.gamingusers.users.presentation.view

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.williamhill.gamingusers.users.presentation.model.UserItem

@OptIn(ExperimentalMaterial3Api::class)
@Composable
internal fun UserItem(
    modifier: Modifier = Modifier,
    user: UserItem.User,
    onClick: () -> Unit = {},
) {
    Surface(
        tonalElevation = 1.dp,
        shadowElevation = 0.dp,
        shape = MaterialTheme.shapes.small,
        modifier = modifier.fillMaxWidth(),
        onClick = onClick
    ) {
        Column(
            modifier = Modifier.padding(PaddingValues(16.dp))
        ) {
            UserItemHeadline(user = user)
            Spacer(modifier = Modifier.size(8.dp))
            UserItemBody(user = user)
        }
    }
}

@Preview
@Composable
private fun PreviewUserItem() {
    val user = UserItem.User(
        id = "user1",
        name = "Stephens Church",
        balance = "$1,817.19",
        email = "stephens.church@williamhill.com",
        age = 24,
        phone = "+1 (954) 549-2384",
        isActive = true,
    )

    UserItem(user = user)
}
