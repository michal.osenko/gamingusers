package com.williamhill.gamingusers.users.presentation.contract

sealed class UsersIntent {

    object LoadUsers : UsersIntent()
    data class NavigateToUser(val id: String) : UsersIntent()
}
