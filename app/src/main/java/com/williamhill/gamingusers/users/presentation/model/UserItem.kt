package com.williamhill.gamingusers.users.presentation.model

import com.williamhill.gamingusers.core.data.remote.model.UserListItemModel

sealed class UserItem {
    class User(
        val id: String,
        val age: Int,
        val balance: String,
        val email: String,
        val isActive: Boolean,
        val name: String,
        val phone: String,
    ): UserItem()

    object Skeleton: UserItem()

}

fun UserListItemModel.toUser() = UserItem.User(
    id = id,
    age = age,
    balance = balance,
    email = email,
    isActive = isActive,
    name = name,
    phone = phone,
)
