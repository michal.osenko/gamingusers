package com.williamhill.gamingusers.users.presentation.view

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.williamhill.gamingusers.users.presentation.model.UserItem

@Composable
internal fun UserItemHeadline(
    modifier: Modifier = Modifier,
    user: UserItem.User,
) {
    Column(
        modifier = modifier.padding(4.dp),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.Start,
    ) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween,
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.Start,
            ) {
                Text(
                    text = user.name,
                    style = MaterialTheme.typography.headlineMedium,
                )
                Text(
                    modifier = Modifier.padding(top = 4.dp),
                    text = " (${user.age})",
                    style = MaterialTheme.typography.titleLarge
                        .copy(color = MaterialTheme.colorScheme.primary.copy(alpha = 0.4f)),
                )
            }
            ActiveIcon(
                modifier = Modifier.padding(horizontal = 12.dp),
                color = if (user.isActive) Color.Green else Color.Red,
            )
        }
        Spacer(modifier = Modifier.size(4.dp))
        Text(
            text = user.balance,
            style = MaterialTheme.typography.headlineSmall,
        )
    }
}

@Preview
@Composable
private fun PreviewUserItemHeadline() {
    val user = UserItem.User(
        id = "user1",
        name = "Stephens Church",
        balance = "$1,817.19",
        email = "stephens.church@williamhill.com",
        age = 24,
        phone = "+1 (954) 549-2384",
        isActive = true,
    )

    UserItemHeadline(user = user)
}
