package com.williamhill.gamingusers.users.presentation.contract

sealed class UsersViewEvent {

    data class NavigateToUser(val id: String) : UsersViewEvent()
}
