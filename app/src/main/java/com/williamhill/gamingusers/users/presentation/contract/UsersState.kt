package com.williamhill.gamingusers.users.presentation.contract

import com.williamhill.gamingusers.users.presentation.model.UserItem

data class UsersState(

    val users: List<UserItem> = listOf(),
    val errorMessage: String = "",
)
