package com.williamhill.gamingusers.users.presentation.view

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.williamhill.gamingusers.R
import com.williamhill.gamingusers.users.presentation.contract.UsersIntent
import com.williamhill.gamingusers.users.presentation.contract.UsersState
import com.williamhill.gamingusers.users.presentation.contract.UsersViewEvent
import com.williamhill.gamingusers.users.presentation.model.UserItem
import com.williamhill.gamingusers.users.presentation.viewmodel.UsersViewModel

@Composable
fun UsersScreen(
    viewModel: UsersViewModel = hiltViewModel(),
    onEvent: (UsersViewEvent) -> Unit
) {
    val viewState = viewModel.uiState.collectAsState()

    LaunchedEffect(Unit) {
        viewModel.uiEvent.collect {
            onEvent(it)
        }
    }

    UsersView(
        state = viewState.value,
        onUserClicked = { viewModel.handleIntent(UsersIntent.NavigateToUser(it)) }
    )
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
private fun UsersView(
    modifier: Modifier = Modifier,
    state: UsersState,
    onUserClicked: (String) -> Unit = {},
) {
    Scaffold(
        topBar = {
            SmallTopAppBar(
                title = {
                    Text(text = stringResource(id = R.string.app_name))
                }
            )
        },
        content = { innerPadding ->
            LazyColumn(
                contentPadding = innerPadding,
                verticalArrangement = Arrangement.spacedBy(8.dp),
                modifier = modifier.fillMaxHeight(),
            ) {
                items(items = state.users, itemContent = {
                    when (it) {
                        UserItem.Skeleton -> UserLoadingView()
                        is UserItem.User -> UserItem(
                            modifier = Modifier,
                            user = it,
                        ) { onUserClicked(it.id) }
                    }
                })
            }
        }
    )
}

@Preview
@Composable
fun PreviewUsersView() {
    val users = listOf(
        UserItem.User(
            id = "user1",
            name = "Stephens Church",
            balance = "$1,817.19",
            email = "stephens.church@williamhill.com",
            age = 24,
            phone = "+1 (954) 549-2384",
            isActive = true,
        ),
        UserItem.User(
            id = "user2",
            name = "Cline Palmer",
            balance = "$3,406.68",
            email = "clinepalmer@enomen.com",
            age = 24,
            phone = "+1 (954) 428-3777",
            isActive = false,
        ),
        UserItem.User(
            id = "user3",
            name = "Shauna Miles",
            balance = "$1,284.17",
            email = "shaunamiles@extremo.com",
            age = 39,
            phone = "+1 (954) 449-3646",
            isActive = true,
        ),
    )

    UsersView(state = UsersState(users = users))
}

@Preview
@Composable
private fun PreviewUsersLoading() {
    val users = arrayListOf<UserItem.Skeleton>().apply {
        for (i in 0..5) add(UserItem.Skeleton)
    }

    UsersView(state = UsersState(users = users)) { }
}
