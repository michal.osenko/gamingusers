package com.williamhill.gamingusers.users.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.williamhill.gamingusers.core.data.UsersRepository
import com.williamhill.gamingusers.core.data.remote.ResponseResult
import com.williamhill.gamingusers.core.data.remote.model.UserListItemModel
import com.williamhill.gamingusers.users.presentation.contract.UsersIntent
import com.williamhill.gamingusers.users.presentation.contract.UsersState
import com.williamhill.gamingusers.users.presentation.contract.UsersViewEvent
import com.williamhill.gamingusers.users.presentation.model.UserItem
import com.williamhill.gamingusers.users.presentation.model.toUser
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import java.util.Collections
import javax.inject.Inject

@HiltViewModel
class UsersViewModel @Inject constructor(
    private val usersRepository: UsersRepository,
) : ViewModel() {

    private val uiInitialState: UsersState
        get() = UsersState(users = Collections.nCopies(20, UserItem.Skeleton))
    private val _uiState: MutableStateFlow<UsersState> = MutableStateFlow(uiInitialState)
    val uiState = _uiState.asStateFlow()
    private val _uiEvent = MutableSharedFlow<UsersViewEvent>()
    val uiEvent = _uiEvent.asSharedFlow()

    init {
        handleIntent(UsersIntent.LoadUsers)
    }

    fun handleIntent(intent: UsersIntent) = viewModelScope.launch {
        _uiState.value = when (intent) {
            is UsersIntent.LoadUsers -> reduceUsersResponseToUsersState(usersRepository.getUsers())
            is UsersIntent.NavigateToUser -> {
                _uiEvent.emit(UsersViewEvent.NavigateToUser(intent.id))
                uiState.value
            }
        }
    }

    private fun reduceUsersResponseToUsersState(
        response: ResponseResult<List<UserListItemModel>>,
    ): UsersState = when (response) {
        is ResponseResult.Success ->
            uiState.value.copy(users = response.data.map { user -> user.toUser() })
        is ResponseResult.Error ->
            uiState.value.copy(errorMessage = response.message)
    }
}
