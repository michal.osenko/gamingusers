package com.williamhill.gamingusers.users.presentation.view

import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable

@Composable
internal fun BodyText(
    text: String,
) {
    Text(
        text = text,
        style = MaterialTheme.typography.titleMedium
            .copy(color = MaterialTheme.colorScheme.primary.copy(alpha = 0.6f)),
    )
}
