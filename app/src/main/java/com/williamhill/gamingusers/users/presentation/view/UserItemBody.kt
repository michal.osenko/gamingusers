package com.williamhill.gamingusers.users.presentation.view

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Email
import androidx.compose.material.icons.rounded.Phone
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.williamhill.gamingusers.users.presentation.model.UserItem

@Composable
internal fun UserItemBody(
    modifier: Modifier = Modifier,
    user: UserItem.User,
) {
    Column(
        modifier = modifier.padding(4.dp),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.Start,
    ) {
        Row {
            Icon(
                Icons.Rounded.Email,
                contentDescription = "Email",
                tint = MaterialTheme.colorScheme.primary.copy(alpha = 0.2f),
            )
            Spacer(modifier = Modifier.size(16.dp))
            BodyText(text = user.email)
        }
        Spacer(modifier = Modifier.size(8.dp))
        Row {
            Icon(
                Icons.Rounded.Phone,
                contentDescription = "Phone",
                tint = MaterialTheme.colorScheme.primary.copy(alpha = 0.2f),
            )
            Spacer(modifier = Modifier.size(16.dp))
            BodyText(text = user.phone)
        }
    }
}

@Preview
@Composable
private fun PreviewUserItemBody() {
    val user = UserItem.User(
        id = "user1",
        name = "Stephens Church",
        balance = "$1,817.19",
        email = "stephens.church@williamhill.com",
        age = 24,
        phone = "+1 (954) 549-2384",
        isActive = true,
    )

    UserItemBody(user = user)
}
