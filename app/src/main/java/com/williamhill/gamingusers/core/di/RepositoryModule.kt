package com.williamhill.gamingusers.core.di

import com.williamhill.gamingusers.core.data.UsersRepository
import com.williamhill.gamingusers.core.data.UsersRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
class RepositoryModule {

    @Provides
    @Singleton
    fun provideUsersRepository(usersRepository: UsersRepositoryImpl): UsersRepository =
        usersRepository

}
