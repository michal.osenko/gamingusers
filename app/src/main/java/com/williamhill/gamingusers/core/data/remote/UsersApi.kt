package com.williamhill.gamingusers.core.data.remote

import com.williamhill.gamingusers.core.data.remote.model.UserListItemModel
import retrofit2.Response
import retrofit2.http.GET

interface UsersApi {

    @GET("users.json")
    suspend fun getUsers(): Response<List<UserListItemModel>>

}
