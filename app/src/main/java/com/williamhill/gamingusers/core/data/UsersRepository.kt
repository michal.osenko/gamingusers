package com.williamhill.gamingusers.core.data

import com.williamhill.gamingusers.core.data.remote.ResponseResult
import com.williamhill.gamingusers.core.data.remote.model.UserListItemModel

interface UsersRepository {

    suspend fun getUsers(): ResponseResult<List<UserListItemModel>>
}
