package com.williamhill.gamingusers.core.data.remote.model

import com.squareup.moshi.Json

data class UserListItemModel(
    @Json(name = "_id")
    val id: String,
    val age: Int,
    val balance: String,
    val email: String,
    val isActive: Boolean,
    val name: String,
    val phone: String,
)
