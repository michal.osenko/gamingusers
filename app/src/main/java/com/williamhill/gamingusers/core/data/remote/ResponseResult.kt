package com.williamhill.gamingusers.core.data.remote

sealed class ResponseResult<T> {

    data class Success<T>(val data: T) : ResponseResult<T>()
    data class Error<T>(val message: String) : ResponseResult<T>()
}
