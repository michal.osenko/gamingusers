package com.williamhill.gamingusers.core.data

import com.williamhill.gamingusers.core.data.remote.ResponseResult
import com.williamhill.gamingusers.core.data.remote.UsersApi
import com.williamhill.gamingusers.core.data.remote.model.UserListItemModel
import com.williamhill.gamingusers.core.di.qualifiers.IoDispatcher
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

class UsersRepositoryImpl @Inject constructor(
    private val usersApi: UsersApi,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher,
) : UsersRepository {

    override suspend fun getUsers(): ResponseResult<List<UserListItemModel>> =
        withContext(ioDispatcher) {
            with(usersApi.getUsers()) {
                val responseBody = body()
                if (isSuccessful && responseBody != null) {
                    ResponseResult.Success(responseBody)
                } else {
                    ResponseResult.Error(message())
                }
            }
        }
}
