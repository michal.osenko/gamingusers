package com.williamhill.gamingusers.core.presentation.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.darkColorScheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import com.williamhill.unitygaming.ui.theme.*


private val DarkColorPalette = darkColorScheme(
    primary = blueBlack,
    primaryContainer = blueDarker,
    secondary = yellowLight,
    secondaryContainer = yellow,
    background = Color.Black,
    surface = Color.Black,
    error = red,
    onPrimary = Color.White,
    onSecondary = blueDark,
    onBackground = Color.White,
    onSurface = Color.White,
    onError = Color.Black,
)

private val LightColorPalette = lightColorScheme(
    primary = blue,
    primaryContainer = blueDarker,
    secondary = yellow,
    secondaryContainer = yellowDark,
    background = Color.White,
    surface = Color.White,
    error = red,
    onPrimary = Color.White,
    onSecondary = blueDark,
    onBackground = blueGray,
    onSurface = blueGray,
    onError = Color.Black,
)

@Composable
fun UnityTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable () -> Unit,
) {
    MaterialTheme(
        colorScheme = if (darkTheme) DarkColorPalette else LightColorPalette,
        typography = typography,
        content = content
    )
}
