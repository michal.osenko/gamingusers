package com.williamhill.gamingusers

import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import com.williamhill.gamingusers.core.presentation.theme.UnityTheme
import com.williamhill.gamingusers.users.presentation.contract.UsersViewEvent
import com.williamhill.gamingusers.users.presentation.view.UsersScreen
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class GamingUsersActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            UnityTheme {
                UsersScreen(onEvent = { event ->
                    if (event is UsersViewEvent.NavigateToUser) {
                        // TODO replace this toast with redirection to the user details
                        Toast.makeText(
                            this@GamingUsersActivity,
                            "The user with id: ${event.id} clicked",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                })
            }
        }
    }
}
