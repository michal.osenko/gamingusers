## Live coding interview
### Technical requirements
- Android Studio v. Chipmunk: [link](https://developer.android.com/studio)
- Android API 32 (prepare `local.properties` file with `sdk.dir` specified)
- Kotlin version 1.6.21
- Java version 1.8
- You are going to share your screen during the interview and the code needs to be visible. Suggestions:
  - Use Presentation Mode in Android Studio: [link](https://www.jetbrains.com/help/idea/ide-viewing-modes.html)
  - Increase the font size of Android Studio
  - Decrease your screen resolution in case it is too high

### Task
Show the details of each user on an individual screen preferably using MVI architecture.
You will need to:
- Add new endpoint to request the details of a single user.
  - Example response: [link](https://gitlab.com/-/snippets/2357431/raw/main/1.json)
  - Generic endpoint: `https://gitlab.com/-/snippets/2357431/raw/main/{userId}.json`
- Add new model for the user details, to parse the API response. 
- Handle navigation between the list screen and the details screen, preferably using Compose Navigation.
- Create a layout, preferably using Jetpack Compose using the following fields from the model:
  - name, age, balance, email, phone, about.
- Suggested UI:

<img src="app/src/main/res/drawable/detail_view_example.jpg" alt="Details view example" style="height:245.87px; width:113.5px;"/>


### Task goals
#### Required
- Show confidence with the code written and decisions taken
- Understand the technologies used
#### Desired
- Follow MVI architecture
- Use the technologies requested, i.e.: Build the UI using Jetpack Compose
- Complete the task
- Create unit tests
